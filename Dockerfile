from golang:1.10
RUN mkdir /go/src/hello
WORKDIR /go/src/hello
ADD ./hello.go ./hello_test.go ./entrypoint.sh ./names.db ./
ADD ./vendor ./vendor
RUN go build -o ./hello
RUN chmod +x ./entrypoint.sh
ENTRYPOINT ["sh", "./entrypoint.sh"]
