build:
	docker build . -t registry.gitlab.com/hamed1soleimani/vm-farms-hello

run: build
	docker run -p 8080:8080 registry.gitlab.com/hamed1soleimani/vm-farms-hello

test: build
	docker run -e TEST=1 registry.gitlab.com/hamed1soleimani/vm-farms-hello

push: build
	docker push registry.gitlab.com/hamed1soleimani/vm-farms-hello

deploy:
	kubectl create -f manifests
