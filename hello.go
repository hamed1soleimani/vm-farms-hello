package main

import (
	"database/sql"
	_ "github.com/lib/pq"
	"io"
	"log"
	"net/http"
	"os"
	"time"
)

var addName string = `INSERT INTO "names" (name, created) values(?,?)`

func main() {
	logger := log.New(os.Stderr, "hello-service: ", log.LstdFlags)
	db, err := sql.Open("postgres", os.Getenv("POSTGRES_URL")
	if err != nil {
		logger.Fatal("Couldn't connect to DB:", err)
	}
	defer db.Close()

	http.HandleFunc("/hello", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/plain; charset=utf-8")
		w.WriteHeader(http.StatusOK)
		name, ok := r.URL.Query()["name"]
		if ok && len(name) > 0 && len(name[0]) > 0 {
			_, err := db.Exec(addName, name[0], time.Now())
			if err != nil {
				logger.Println("Couldn't write name to DB:", err)
			}
			io.WriteString(w, "Hello "+name[0]+"!")
		} else {
			io.WriteString(w, "Hello stranger!")
		}
	})
	logger.Fatal(http.ListenAndServe(":8080", nil))
}
