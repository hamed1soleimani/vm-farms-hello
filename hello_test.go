package main

import (
	"database/sql"
	"io/ioutil"
	"net/http"
	"testing"
	"time"
)

func openDB(t *testing.T) *sql.DB {
	db, err := sql.Open("sqlite3", "./names.db")
	if err != nil {
		t.Fatal("Couldn't connect to DB:", err)
	}
	return db
}

func request(t *testing.T, param string) string {
	resp, err := http.Get("http://localhost:8080/hello" + param)
	if err != nil {
		t.Fatal("Http request failed:", err)
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		t.Fatal("Invalid http status code:", resp.StatusCode)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal("Invalid response contents:", err)
	}

	return string(body)
}

func countRows(t *testing.T, db *sql.DB) uint64 {
	res, err := db.Query("SELECT COUNT(*) FROM names")
	if err != nil {
		t.Fatal("Couldn't count db rows!", err)
	}
	defer res.Close()
	var count uint64
	res.Next()
	err = res.Scan(&count)
	if err != nil {
		t.Fatal("Couldn't unmarshall counting results:", err)
	}
	return count
}

func TestRouteAssumesEmptyNameAsStranger(t *testing.T) {
	db := openDB(t)
	defer db.Close()
	count := countRows(t, db)

	if request(t, "?name") != "Hello stranger!" {
		t.Fatalf("Request with empty name should be considered as starnger")
	}

	count1 := countRows(t, db)

	if count1 != count {
		t.Fatal("An starnger shouldn't add anything to our DB!")
	}
}

func TestRouteAssumesEmptyParamsAsStranger(t *testing.T) {
	db := openDB(t)
	defer db.Close()
	count := countRows(t, db)

	if request(t, "") != "Hello stranger!" {
		t.Fatalf("Request with empty name should be considered as starnger")
	}

	count1 := countRows(t, db)

	if count1 != count {
		t.Fatal("An starnger shouldn't add anything to our DB!")
	}
}

func TestRouteWorksWithValidName(t *testing.T) {
	db := openDB(t)
	defer db.Close()
	count := countRows(t, db)

	if request(t, "?name=Alice") != "Hello Alice!" {
		t.Fatalf("Request with empty name should be considered as starnger")
	}

	count1 := countRows(t, db)

	if count1 != count+1 {
		t.Fatal("An starnger shouldn't add one row to our DB!")
	}

	type Name struct {
		ID      int       `json:"id"`
		Name    string    `json:"name"`
		Created time.Time `json:"created"`
	}

	res, err := db.Query("SELECT * from names where id=?", count1)
	if err != nil {
		t.Fatal("Couldn't load db rows!", err)
	}
	defer res.Close()
	var name Name
	if res.Next() {
		err = res.Scan(&name.ID, &name.Name, &name.Created)
		if err != nil {
			t.Fatal("Couldn't unmarshall counting results:", err)
		}
	} else {
		t.Fatal("Couldn't find added name in DB!")
	}

	diff := time.Now().Sub(name.Created).Seconds()

	if name.Name != "Alice" || name.ID != int(count1) || diff < 0 || diff > 10 {
		t.Fatal("Unexpected value in names DB!")
	}
}
